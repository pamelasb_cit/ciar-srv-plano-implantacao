# Plano de implanta��o 

O projeto automatiza a cria��o do plano de implanta��o para RDMs.

## Input
Arquivo .csv com a lista de microsservi�os, vers�o base e vers�o can�rio a serem implantadas e vers�o base e vers�o can�rio para o procedimento de retorno.

Exemplo de arquivo .csv:
```sh
nome do microsservi�o,VERSAO_BASE,VERSAO_CANARIO,VERSAO_BASE_RETORNO,VERSAO_CANARIO_RETORNO
```

- Caso o microsservi�o n�o tenha can�rio
```sh
nome do microsservi�o,VERSAO_BASE,,VERSAO_BASE_RETORNO,
```

- Caso esteja removendo o can�rio
```sh
nome do microsservi�o,VERSAO_BASE,,VERSAO_BASE_RETORNO,VERSAO_CANARIO_RETORNO
```

- Caso seja a primeira implanta��o do microsservi�o e ainda n�o tenha vers�o de retorno
```sh
nome do microsservi�o,VERSAO_BASE,,,
```

## Output

- Arquivo .txt do plano de implanta��o com as tarefas a serem executadas na RDM e as vers�es dos microsservi�os. 

- As evid�ncias para serem anexas na RDM:
 
    - Screenshot do prod deploy do Bamboo. 
    - Screenshot da an�lise do Sonar.

> Nota: Necess�rio ter instalado o Webdriver do Chrome ou Firefox, pois o projeto utiliza o Selenium para coletar as evid�ncias. 

## Comandos �teis
```sh

# Build do projeto:
mvn clean build

# Gerar pacote jar
mvn package

# Argumentos para a execu��o do jar
-Dwebdriver.chrome={caminho do webdriver do chrome}

# OU
-Dwebdriver.firefox={caminho do webdriver do firefox}

# Executando o projeto via linha de comando
java -Dwebdriver.chrome={caminho do webdriver do chrome} -jar nome-do-jar.jar {chave m} {senha} {arquivo csv} {diretorio para evidencias}

# Executando o projeto via Intellij
VM Options: -Dwebdriver.chrome={caminho do webdriver do chrome}
Program Arguments: {chave m} {senha} {arquivo csv} {diretorio para evidencias}

```
