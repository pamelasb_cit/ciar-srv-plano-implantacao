package br.com.bradesco.plano.implantacao.configuration;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@Slf4j
public class WebDriverConfig {

    @Value("${webdriver.chrome:}")
    private String chromeWebDriver;

    @Value("${webdriver.firefox:}")
    private String firefoxWebDriver;

    @Bean
    public WebDriver browser() {

        if (StringUtils.isNotBlank(chromeWebDriver)) {
            System.setProperty("webdriver.chrome.driver", chromeWebDriver);
            log.info("Utilizando o Chrome");
            return new ChromeDriver();
        }
        if (StringUtils.isNotBlank(firefoxWebDriver)) {
            System.setProperty("webdriver.gecko.driver", firefoxWebDriver);
            log.info("Utilizando o Firefox");
            return new FirefoxDriver();
        }

        log.error("Erro ao configurar o webdriver!");
        throw new RuntimeException("Webdriver exception.");
    }
}
