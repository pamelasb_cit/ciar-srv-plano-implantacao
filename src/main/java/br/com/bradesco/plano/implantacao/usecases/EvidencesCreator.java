package br.com.bradesco.plano.implantacao.usecases;

import br.com.bradesco.plano.implantacao.entities.Microservice;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
@Slf4j
public class EvidencesCreator {

    private final WebDriver browser;
    private final ScreenshotTaker screenshotTaker;

    public void execute(String chaveM, String password, List<Microservice> microserviceList, String evidencesDir) {

        log.info("Coletando evidencias ...");

        try {
            browser.manage().window().maximize();
            loginBamboo(chaveM, password);
            loginSonar(chaveM, password);

            microserviceList.forEach(microservice -> {
                takeBambooEvidence(microservice, evidencesDir);
                takeSonarEvidence(microservice, evidencesDir);
            });
        } catch (Exception e) {
            log.error("Erro ao coletar evidencias {}", e);
        } finally {
            browser.quit();
        }

        log.info("Evidencias coletadas!");
    }

    private void takeBambooEvidence(Microservice microservice, String evidencesDir) {
        switchBambooTab();

        //procura aplicacao
        WebElement quickSearchField = browser.findElement(By.cssSelector("input[class='term text']"));
        quickSearchField.sendKeys(microservice.getName());
        waitForLoad(browser, 2);
        quickSearchField.sendKeys(Keys.ENTER);
        waitForLoad(browser, 2);

        //clica em deployments
        String currentDeploymentsUrl = browser.getCurrentUrl();
        String applicationId = StringUtils.substringAfterLast(currentDeploymentsUrl, "/");
        String deploymentsId = "deployments:" + applicationId;
        browser.findElement(By.id(deploymentsId)).click();
        waitForLoad(browser, 2);

        //clica em PR
        browser.findElement(By.xpath("//a[text()='PR']")).click();
        waitForLoad(browser, 2);
        String currentPRUrl = browser.getCurrentUrl();

        findVersionAndTakeBambooEvidence(microservice.getName(), microservice.getBaseVersion(), evidencesDir);
        microservice.setSonarLink(getSonarLink());

        if (microservice.hasCanaryVersion()) {
            browser.get(currentPRUrl);
            findVersionAndTakeBambooEvidence(microservice.getName(), microservice.getCanaryVersion(), evidencesDir);
        }
    }

    private void findVersionAndTakeBambooEvidence(String applicationName, String applicationVersion, String evidencesDir) {
        try {
            // clica na versão
            browser.findElement(By.xpath("//a[text()='" + applicationVersion + "']")).click();
            waitForLoad(browser, 2);

            //pega o span que contém o link da execução da build
            WebElement spanElement = browser.findElement(By.cssSelector("span[class='artifacts-from-result']"));

            // clica na execução da build
            List<WebElement> linkList = spanElement.findElements(By.tagName("a"));
            for (WebElement webElement : linkList) {
                if (webElement.getText().contains("#")) {
                    webElement.click();
                    break;
                }
            }
            waitForLoad(browser, 2);

            screenshotTaker.execute(applicationName + "-" + applicationVersion + "-prod-deploy.png", evidencesDir, browser);

        } catch (Exception e) {
            log.error("Versao {} da aplicacao {} nao esta disponivel em PR", applicationVersion, applicationName);
        }
    }

    private String getSonarLink() {
        String sonarLink = null;
        try {
            sonarLink = browser.findElement(By.xpath("//a[text()='SonarQube analysis results']")).getAttribute("href");
        } catch (Exception e) {
            log.error("Link SonarQube analysis results nao encontrado!");
        }
        return sonarLink;
    }

    private void takeSonarEvidence(Microservice microservice, String evidencesDir) {
        if (StringUtils.isBlank(microservice.getSonarLink())) {
            log.warn("Sonar link da aplicacao {} esta vazio", microservice.getName());
            return;
        }

        switchSonarTab();
        browser.get(microservice.getSonarLink());
        waitForLoad(browser, 5);
        screenshotTaker.execute(microservice.getName() + "-sonar.png", evidencesDir, browser);
    }

    /**
     * Fecha div de alerta no topo da tela do Bamboo, se existir.
     *
     * @param browser
     */
    private void closeBanner(WebDriver browser) {
        try {
            WebElement closeButton = browser.findElement(By.cssSelector("span[class='aui-icon icon-close']"));
            if (closeButton != null) {
                closeButton.click();
            }
        } catch (Exception e) {
            log.warn("Erro ao fechar o banner de alerta do Bamboo, elemento nao existente.");
        }
    }

    private void loginBamboo(String chaveM, String password) {
        browser.get("https://bamboo.bradesco.com.br:8443/allPlans.action");
        WebElement usernameIdField = browser.findElement(By.id("loginForm_os_username"));
        usernameIdField.sendKeys(chaveM);

        WebElement passwordIdField = browser.findElement(By.id("loginForm_os_password"));
        passwordIdField.sendKeys(password);

        WebElement submitButton = browser.findElement(By.id("loginForm_save"));

        closeBanner(browser);
        submitButton.click();
    }

    private void loginSonar(String chaveM, String password) {
        ((JavascriptExecutor) browser).executeScript("window.open()");
        switchSonarTab();
        browser.get("https://sonarqube.bradesco.com.br:8443/sonarqube");
        waitForLoad(browser, 5);

        WebElement usernameIdField = browser.findElement(By.id("login"));
        usernameIdField.sendKeys(chaveM);

        WebElement passwordIdField = browser.findElement(By.id("password"));
        passwordIdField.sendKeys(password);

        WebElement submitButton = browser.findElement(By.xpath("//button[text()='Log in']"));
        submitButton.click();
    }

    private void switchSonarTab() {
        List<String> tabs = new ArrayList<>(browser.getWindowHandles());
        browser.switchTo().window(tabs.get(1));
    }

    private void switchBambooTab() {
        List<String> tabs = new ArrayList<>(browser.getWindowHandles());
        browser.switchTo().window(tabs.get(0));
    }

    private void waitForLoad(WebDriver driver, int seconds) {
        ExpectedCondition<Boolean> expectation = new
                ExpectedCondition<Boolean>() {
                    public Boolean apply(WebDriver driver) {
                        return "complete".equals(((JavascriptExecutor) driver).executeScript("return document.readyState").toString());
                    }
                };
        try {
            Thread.sleep(seconds * 1000l);
            WebDriverWait wait = new WebDriverWait(driver, 30);
            wait.until(expectation);
        } catch (Exception e) {
            log.error("Timeout waiting for Page Load Request to complete.");
        }
    }
}
