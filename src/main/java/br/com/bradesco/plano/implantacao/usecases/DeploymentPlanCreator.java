package br.com.bradesco.plano.implantacao.usecases;

import br.com.bradesco.plano.implantacao.entities.Microservice;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.ResourceUtils;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

@Service
@Slf4j
public class DeploymentPlanCreator {

    private static final String CLEANING_PREVIOUS_CANARY_TASK_TEMPLATE = "tarefa-1-limpeza-canario-template.txt";
    private static final String DEPLOY_BASE_TASK_TEMPLATE = "tarefa-2.1-instalacao-base-template.txt";
    private static final String RETURN_CANARY_AND_BASE_TASK_TEMPLATE = "tarefa-2.2-procedimento-retorno-canario-e-base-template.txt";
    private static final String RETURN_BASE_TASK_TEMPLATE = "tarefa-2.3-procedimento-retorno-base-template.txt";
    private static final String DEPLOY_CANARY_TASK_TEMPLATE = "tarefa-3.1-instalacao-canario-template.txt";
    private static final String CLEANING_CURRENT_CANARY_TASK_TEMPLATE = "tarefa-3.2-procedimento-retorno-canario-template.txt";
    private static final String RETURN_CANARY_TASK_TEMPLATE = "tarefa-3.3-procedimento-retorno-base-e-canario-template.txt";
    private final StringBuilder cleanPreviousCanaryTask = new StringBuilder();
    private final StringBuilder deployBaseTask = new StringBuilder();
    private final StringBuilder returnBaseAndCanaryTask = new StringBuilder();
    private final StringBuilder returnBaseTask = new StringBuilder();
    private final StringBuilder deployCanaryTask = new StringBuilder();
    private final StringBuilder cleanCurrentCanaryTask = new StringBuilder();
    private final StringBuilder returnCanaryTask = new StringBuilder();
    private static final String NEW_LINE = System.getProperty("line.separator");

    /**
     * Cria arquivo txt de plano de implantação com as versões a serem implantadas e versões de rollback.
     *
     * @param microserviceList
     * @param evidencesDir
     */
    public void execute(List<Microservice> microserviceList, String evidencesDir) {

        log.info("Criando plano de implantacao ...");

        microserviceList.forEach(a -> {
            createCleaningPreviousCanaryTask(a);
            createDeployBaseTask(a);
            createReturnBaseAndCanaryTask(a);
            createReturnBaseTask(a);
            createDeployCanaryTask(a);
            createCleaningCurrentCanaryTask(a);
            createReturnCanaryTask(a);
        });

        createDeploymentPlan(evidencesDir);

        log.info("Plano de implantacao criado!");
    }

    private void createDeploymentPlan(String evidencesDir) {

        StringBuilder content = new StringBuilder();
        try {
            content.append(readTemplateFile(CLEANING_PREVIOUS_CANARY_TASK_TEMPLATE));
            content.append(cleanPreviousCanaryTask);

            content.append(readTemplateFile(DEPLOY_BASE_TASK_TEMPLATE));
            content.append(deployBaseTask);

            content.append(readTemplateFile(RETURN_CANARY_AND_BASE_TASK_TEMPLATE));
            content.append(returnBaseAndCanaryTask);

            content.append(readTemplateFile(RETURN_BASE_TASK_TEMPLATE));
            content.append(returnBaseTask);

            content.append(readTemplateFile(DEPLOY_CANARY_TASK_TEMPLATE));
            content.append(deployCanaryTask);

            content.append(readTemplateFile(CLEANING_CURRENT_CANARY_TASK_TEMPLATE));
            content.append(cleanCurrentCanaryTask);

            content.append(readTemplateFile(RETURN_CANARY_TASK_TEMPLATE));
            content.append(returnCanaryTask);

        } catch (Exception e) {
            log.error("Erro ao manipular o arquivo de template!");
            throw new RuntimeException("File error");
        }
        writeTxtFile(content.toString(), evidencesDir);
    }

    private String readTemplateFile(String templateName) throws IOException {
        File file = ResourceUtils.getFile("classpath:templates/" + templateName);
        List<String> strings = Files.readAllLines(file.toPath(), StandardCharsets.UTF_8);
        strings.add(NEW_LINE);
        return StringUtils.join(strings, NEW_LINE);
    }

    private void writeTxtFile(String content, String evidencesDir) {
        try {
            validateDir(evidencesDir);
            Files.write(Paths.get(evidencesDir + "plano-implantacao.txt"), content.getBytes());
        } catch (IOException e) {
            log.error("Erro ao escrever o arquivo de Plano de Implantacao!");
            throw new RuntimeException("File error");
        }
    }

    private void validateDir(String evidencesDir) {
        File dir = new File(evidencesDir);
        if (!dir.exists()) {
            dir.mkdir();
            log.warn("Diretorio de evidencias criado!");
        }
    }

    private void createReturnCanaryTask(Microservice microservice) {
        if (!microservice.hasCanaryReturnVersion() || !microservice.hasCanaryVersion()) {
            return;
        }

        String previousBaseVersionCommand = "\t"
                .concat(microservice.getName())
                .concat("\t\t\t \t        base anterior ")
                .concat(microservice.getBaseReturnVersion());

        String previousCanaryVersionCommand = "\t"
                .concat(microservice.getName())
                .concat("\t\t\t \t        canário anterior ")
                .concat(microservice.getCanaryReturnVersion());

        returnCanaryTask.append(previousBaseVersionCommand);
        returnCanaryTask.append(NEW_LINE);

        returnCanaryTask.append(previousCanaryVersionCommand);
        returnCanaryTask.append(NEW_LINE);
    }

    private void createDeployCanaryTask(Microservice microservice) {
        if (!microservice.hasCanaryVersion()) {
            return;
        }

        String command = "\t"
                .concat(microservice.getName())
                .concat("\t\t\t\t\t\t")
                .concat(microservice.getCanaryVersion());

        deployCanaryTask.append(command);
        deployCanaryTask.append(NEW_LINE);
    }

    private void createReturnBaseTask(Microservice microservice) {
        if (!microservice.hasBaseReturnVersion() || microservice.hasCanaryReturnVersion()) {
            return;
        }

        String previousBaseVersionCommand = "\t"
                .concat(microservice.getName())
                .concat("\t\t\t \t        base anterior ")
                .concat(microservice.getBaseReturnVersion());

        returnBaseTask.append(previousBaseVersionCommand);
        returnBaseTask.append(NEW_LINE);
    }

    private void createReturnBaseAndCanaryTask(Microservice microservice) {
        if (!microservice.hasCanaryReturnVersion()) {
            return;
        }

        String previousBaseVersionCommand = "\t"
                .concat(microservice.getName())
                .concat("\t\t\t \t        base anterior ")
                .concat(microservice.getBaseReturnVersion());

        String previousCanaryVersionCommand = "\t"
                .concat(microservice.getName())
                .concat("\t\t\t \t        canário anterior ")
                .concat(microservice.getCanaryReturnVersion());

        returnBaseAndCanaryTask.append(previousBaseVersionCommand);
        returnBaseAndCanaryTask.append(NEW_LINE);

        returnBaseAndCanaryTask.append(previousCanaryVersionCommand);
        returnBaseAndCanaryTask.append(NEW_LINE);
    }

    private void createDeployBaseTask(Microservice microservice) {
        String command = "\t"
                .concat(microservice.getName())
                .concat("\t\t\t\t\t\t")
                .concat(microservice.getBaseVersion());

        deployBaseTask.append(command);
        deployBaseTask.append(NEW_LINE);
    }

    private void createCleaningCurrentCanaryTask(Microservice microservice) {
        if (!microservice.hasCanaryVersion()) {
            return;
        }

        String canaryCleaningCommand = "./canary_v4.sh --clean --namespace pfl2 --project "
                .concat(microservice.getName())
                .concat(" --version ")
                .concat(microservice.getCanaryVersion());

        cleanCurrentCanaryTask.append(canaryCleaningCommand);
        cleanCurrentCanaryTask.append(NEW_LINE);
    }

    private void createCleaningPreviousCanaryTask(Microservice microservice) {
        if (!microservice.hasCanaryReturnVersion()) {
            return;
        }

        String canaryCleaningCommand = "./canary_v4.sh --clean --namespace pfl2 --project "
                .concat(microservice.getName())
                .concat(" --version ")
                .concat(microservice.getCanaryReturnVersion());

        cleanPreviousCanaryTask.append(canaryCleaningCommand);
        cleanPreviousCanaryTask.append(NEW_LINE);
    }
}
