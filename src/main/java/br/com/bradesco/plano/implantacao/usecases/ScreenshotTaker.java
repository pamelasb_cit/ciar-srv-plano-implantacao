package br.com.bradesco.plano.implantacao.usecases;

import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.springframework.stereotype.Service;
import org.springframework.util.FileCopyUtils;

import java.io.File;

@Service
@Slf4j
public class ScreenshotTaker {

    public void execute(String fileName, String evidencesDir, WebDriver browser) {
        try {
            zoomOut(browser);

            File src = ((TakesScreenshot) browser).getScreenshotAs(OutputType.FILE);
            FileCopyUtils.copy(src, new File(evidencesDir + fileName));

            zoomIn(browser);
        } catch (Exception e) {
            log.error("Erro ao tirar print!");
            throw new RuntimeException("Print error", e);
        }
    }

    private void zoomOut(WebDriver browser) {
        JavascriptExecutor executor = (JavascriptExecutor) browser;
        executor.executeScript("document.body.style.zoom = '0.8'");
    }

    private void zoomIn(WebDriver browser) {
        JavascriptExecutor executor = (JavascriptExecutor) browser;
        executor.executeScript("document.body.style.zoom = '1'");
    }
}
