package br.com.bradesco.plano.implantacao.usecases;

import br.com.bradesco.plano.implantacao.entities.Microservice;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
@Slf4j
public class DeploymentPlanProcessor {
    private final CsvExtraction csvExtraction;
    private final DeploymentPlanCreator deploymentPlanCreator;
    private final EvidencesCreator evidenceCreator;

    public void execute(String chaveM, String password, String csvFile, String evidencesDir) {

        List<Microservice> microserviceList = csvExtraction.execute(csvFile);
        microserviceList.forEach(a -> log.info(a.toString()));

        deploymentPlanCreator.execute(microserviceList, evidencesDir);

        evidenceCreator.execute(chaveM, password, microserviceList, evidencesDir);

    }
}
