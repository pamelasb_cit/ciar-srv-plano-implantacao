package br.com.bradesco.plano.implantacao.entities;

import com.opencsv.bean.CsvBindByPosition;
import lombok.Data;
import lombok.ToString;
import org.apache.commons.lang3.StringUtils;

@Data
@ToString(exclude = {"sonarLink"})
public class Microservice {
    private static final String NOT_APPLICABLE = "N/A";

    @CsvBindByPosition(position = 0)
    private String name;

    @CsvBindByPosition(position = 1)
    private String baseVersion;

    @CsvBindByPosition(position = 2)
    private String canaryVersion;

    @CsvBindByPosition(position = 3)
    private String baseReturnVersion;

    @CsvBindByPosition(position = 4)
    private String canaryReturnVersion;

    private String sonarLink;

    public boolean hasCanaryReturnVersion() {
        return !(StringUtils.isBlank(canaryReturnVersion) || NOT_APPLICABLE.equalsIgnoreCase(canaryReturnVersion));
    }

    public boolean hasCanaryVersion() {
        return !(StringUtils.isBlank(canaryVersion) || NOT_APPLICABLE.equalsIgnoreCase(canaryVersion));
    }

    public boolean hasBaseReturnVersion() {
        return !(StringUtils.isBlank(baseReturnVersion) || NOT_APPLICABLE.equalsIgnoreCase(baseReturnVersion));
    }
}
