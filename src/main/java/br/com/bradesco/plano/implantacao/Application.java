package br.com.bradesco.plano.implantacao;

import br.com.bradesco.plano.implantacao.usecases.DeploymentPlanProcessor;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@RequiredArgsConstructor
@Slf4j
public class Application implements ApplicationRunner {

    private final DeploymentPlanProcessor deploymentPlanProcessor;

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {
        validate(args.getSourceArgs());

        String chaveM = args.getSourceArgs()[0];
        String password = args.getSourceArgs()[1];
        String csvFile = args.getSourceArgs()[2];
        String evidencesDir = args.getSourceArgs()[3];

        deploymentPlanProcessor.execute(chaveM, password, csvFile, evidencesDir);
    }

    private static void validate(String[] args){
        if (args.length <= 3) {
            log.error("4 parametros são necessários: [1] chave M, [2] senha, [3] arquivo csv, [4] diretório de evidências.");
            throw new RuntimeException("Invalid parameter");
        }
    }
}
