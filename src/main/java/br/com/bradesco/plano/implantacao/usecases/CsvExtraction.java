package br.com.bradesco.plano.implantacao.usecases;

import br.com.bradesco.plano.implantacao.entities.Microservice;
import com.opencsv.bean.CsvToBeanBuilder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.List;

@Service
@Slf4j
public class CsvExtraction {

    /**
     * Extrai arquivo csv e cria objetos <code>Microservice</code>.
     *
     * @param csvFile
     * @return
     */
    public List<Microservice> execute(String csvFile) {
        try {
            log.info("Extraindo csv ...");
            return new CsvToBeanBuilder(new FileReader(csvFile)).withType(Microservice.class).build().parse();
        } catch (FileNotFoundException e) {
            log.error("Arquivo csv invalido");
            throw new RuntimeException(e);
        }
    }
}
